//本程序用于管理AvenirMQ 相当于终端控制台

const INI = require('ini');
const os = require('os');
const fs = require('fs');
const libcu = require('libcu');
const readLine = require('readline-sync');
const amq = require('avenirmq');
//具备root权限的签名
const sign = libcu.cipher.AesEncode("AvenirMQ");
async function giveFileName() {
    let fileName = "./run.ini"
    return fileName;
}

async function readIni(fileName) {
    let file = fs.readFileSync(fileName);
    var Info = INI.parse(file.toString());
    return Info;
}


async function main() {
    let ini = await readIni(await giveFileName());
    amq.init({
        ip:ini.main.ip,
        port:ini.main.port,
    })
    while (1) {
        let command = readLine.question('AvenirMQ> ');
        let type = command.split(' ');
        if (type[0] === 'help' || !type[0]) {
            console.log(Usage());
        } else if (type[0] === 'addUser') {
            if (type.length != 6) {
                throw ('bad [addUser] command');
            }
            //如果是添加用户的话
            let name = type[1];
            let password = type[2];
            let ip = type[3];
            let port = type[4];
            let key = type[5];
            if (!name || !password  ||!ip || !port ||!key) {
                console.log('lack of params');
                continue;
            }
            let send = {
                type:'addUser',
                data:{
                    name,
                    password:libcu.cipher.AesEncode(password),
                    ip,
                    port,
                    key
                }
            }
            let res = await safeSend(send,'addUser');
            console.log(res);
        } else if (type[0] === 'deleteUser') {
            let name = type[1];
            if (!name) {
                console.log('lack of name');
                continue;
            }
            let send = {
                type:'deleteUser',
                data : {
                    name,
                },
            }
            let res = await safeSend(send,'deleteUser');
            console.log(res);

        }else if (type[0] === 'updateUser') {
            if (type.length != 6) {
                throw ('bad [updateUser] command');
            }
            let name = type[1];
            let password = type[2];
            let ip = type[3];
            let port = type[4];
            let key = type[5];
            if (!name || !password  ||!ip || !port ||!key) {
                console.log('lack of params');
                continue;
            }
            let send = {
                type:'updateUser',
                data:{
                    name,
                    password:libcu.cipher.AesEncode(password),
                    ip,
                    port,
                    key
                }
            }
            let res = await safeSend(send,'updateUser');
            console.log(res);

        }else if (type[0] === 'setKey') {
            if(type.length!=3) {
                console.log("bad [setKey] command");
                continue;
            }
            let name = type[1];
            let key = type[2];
            if (!name||!key) {
                console.log('lack of name or key');
                continue;
            }
            let send = {
                type:'setKey',
                data:{
                    name,
                    key,
                }
            }
            let res = await safeSend(send,'setKey');
            console.log(res);


        } else if (type[0] == 'exit') {
            process.exit(0);
        } else if(type[0] == 'login') {
            if(type.length!=3) {
                console.log('bad [login] command');
                continue;
            }
            let name = type[1];
            let password = type[2];
            if(!name || !password) {
                console.log('lacf of name or password');
                continue;
            }
            let send = {
                type:'login',
                data:{
                    name,
                    password:libcu.cipher.AesEncode(password),
                }
            }
            let res = await safeSend(send,'login');
            console.log(res);
        } else if(type[0] == 'userList') {
            if(type.length!=1) {
                console.log("bad [userList] command");
                continue;
            }
            let send = {
                type:'userList',
                data:{
                }
            }
            let res = await safeSend(send,'login');
            console.log(res);
        } else {
            console.log("sorry,unknown command");
        }
    }
    return 0;

}

//安全的连接操作
async function safeConnect() {
    setTimeout(async () => {
        return 'timeout';
    }, 3000);
    // let sign = await avenirsql.connect();
    return sign;
}

//安全的发送操作
async function safeSend(send,type) {
    let promise = new Promise(async(resolve,reject)=>{
        setTimeout(async () => {
            resolve('timeout');
        }, 3000);
        let res =  await amq.asyncSend(send,type);
        resolve(res);
    })
    return promise;
    
}


function Usage() {
    const use = `\t欢迎使用AvenirMQ管理程序
    \tlogin [name] [password]:                                  获得签名
    \taddUser [name] [password] [ip] [port] [routingkey]:       新建用户
    \tupdateUser [name] [password] [ip] [port] [routingkey]:    更新用户
    \tdeleteUser [name]:                                        删除用户
    \tuserList:                                                 获取用户列表
    \texit:                                                     退出`;
    return use;
}

// function getMessage(res) {
//     return res.message ? res.message : res
// }

main();